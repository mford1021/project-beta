from .models import Technician, Appointment
from common.json import ModelEncoder

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["reason", "status", "vin", "customer", "technician_id", "technician_name", "date_time"]
