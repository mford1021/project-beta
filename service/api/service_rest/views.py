from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound
from .encoders import TechnicianListEncoder, AppointmentListEncoder
from .models import Technician, Appointment
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder)

    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments}, encoder=AppointmentListEncoder)

    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

def cancel_appointment(request, id):
    if request.method == 'PUT':
        appointment = Appointment.objects.filter(id=id).first()
        if not appointment:
            return HttpResponseNotFound()
        appointment.status = "cancelled"
        appointment.save()
        return JsonResponse({"appointment": appointment}, encoder=AppointmentListEncoder)
    return HttpResponseBadRequest()

def finish_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.filter(id=id).first()
        if not appointment:
            return HttpResponseNotFound()
        appointment.status = "finished"
        appointment.save()
        return JsonResponse({"appointment": appointment}, encoder=AppointmentListEncoder)
    return HttpResponseBadRequest()
