from django.db import models
from django.urls import reverse
from django.utils.timezone import now


class Technician(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    employee_id = models.CharField(max_length=25)

    def get_api_url(self):
        return reverse("api_delete_technician", kwargs={"id": self.id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('cancelled', 'Cancelled'),
        ('finished', 'Finished'),
    )
    date_time = models.DateTimeField(default=now)
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=25, choices=STATUS_CHOICES, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=25)
    technician = models.ForeignKey(Technician, related_name="technicians", on_delete=models.CASCADE, default=None)

    def get_api_url(self):
        return reverse("api_delete_appointment", kwargs={"id": self.id})

    @property
    def technician_name(self):
        return f"{self.technician.first_name} {self.technician.last_name}"
