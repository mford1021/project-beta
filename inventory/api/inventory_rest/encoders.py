from common.json import ModelEncoder

from .models import Automobile, Manufacturer, VehicleModel, Feedback


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
    ]


class VehicleModelEncoder(ModelEncoder):
    model = VehicleModel
    properties = [
        "id",
        "name",
        "picture_url",
        "manufacturer",
    ]
    encoders = {
        "manufacturer": ManufacturerEncoder(),
    }


class AutomobileEncoder(ModelEncoder):
    model = Automobile
    properties = [
        "id",
        "color",
        "year",
        "vin",
        "model",
        "sold",
    ]
    encoders = {
        "model": VehicleModelEncoder(),
    }


class FeedbackEncoder(ModelEncoder):
    model = Feedback
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "feedback"
    ]