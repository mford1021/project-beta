from django.urls import path
from .views import sales_list, sale_details, salesperson_list, salesperson_details, customer_list, customer_details


urlpatterns = [
    path("sales/", sales_list, name="sales_list"),
    path("sales/<int:id>", sale_details, name="sale_details"),
    path("salespeople/", salesperson_list, name="salespeople_list"),
    path("salespeople/<int:id>", salesperson_details, name="salesperson_details"),
    path("customers/", customer_list, name="customer_list"),
    path("customers/<int:id>", customer_details, name="customer_details"),
]
