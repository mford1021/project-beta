from rest_framework import serializers
from .models import Salesperson, Sale, Customer

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Customer
        fields=('first_name','last_name','address','phone_number')