from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=25)

    def get_api_url(self):
        return reverse("api_auto", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.vin}"

    class Meta:
        ordering = ["vin"]


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField()


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)


class Sale(models.Model):
    price = models.CharField(max_length=100)
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name="sale"
    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE,
        related_name="sale"
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
        related_name="sale"
    )
