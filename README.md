# CarCar

Team:

* Michael Ford  - Sales microservice
* Paul Pham - Services mircoservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

The models used are Salesperson, Customer, Sales, and AutomobileVO. Sales is made up almost entirely of data from the other three: the customer name, salesperson name, and the VIN (Customer, Salesperson, and AutomobileVO, respectively). The VIN is taken from the Automobile model in the inventory microservice using polling.

In addition, I added a Feedback model to the inventory that I originally wished to create a section for comments from the users to be logged. This was put on the back burner to handle errors that cropped up, but the skeleton is still there for when these problems are solved.

IMPORTANT: React port takes several minutes to fully boot.
