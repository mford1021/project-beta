import React, { useState, useEffect } from "react";

export default function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const cancelAppointment = async(id) => { console.log(id);
      const url = `http://localhost:8080/${id}/cancel`;
      const response = await fetch(url, {method: "PUT"});
      if (response.ok) {
          setAppointments(appointments.filter(appointment => appointment.href !== id));
      }
  };

  const getDateTime = (dateTime) => {
    const inputDate = '2023-04-28T21:55:32.318832+00:00';
    const dateObj = new Date(inputDate);

    const year = dateObj.getFullYear();
    const month = dateObj.getMonth() + 1; // months are zero-indexed, so add 1
    const day = dateObj.getDate();
    const hours = dateObj.getHours();
    const minutes = dateObj.getMinutes();
    const seconds = dateObj.getSeconds();

    const formattedDate = `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`;
    const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

    return {"date" : formattedDate, "time" : formattedTime}

  }

  const finishAppointment = async(id) => { console.log(id);

    const url = `http://localhost:8080/${id}/finish`;
    const response = await fetch(url, {method: "PUT"});
    if (response.ok) {
        setAppointments(appointments.filter(appointment => appointment.href !== id));
    }
};

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-4">
            <div className="row">
                <div className="col-12">
                    <h1 className="text-center mb-4">Appointments</h1>
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Is VIP?</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Cancel/Finish</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.map((appointment) => {
                                return (
                                    <tr key={appointment.href}>
                                        <td>{appointment.vin}</td>
                                        <td>N/A</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.date_time}</td>
                                        <td>{appointment.date_time}</td>
                                        <td>{appointment.technician_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td><button className="btn btn-secondary" onClick={() => cancelAppointment(appointment.href)}>Cancel</button><button className="btn btn-secondary" onClick={() => finishAppointment(appointment.href)}>Finish</button></td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}


// <td>{getDateTime(appointment.date_time).date}</td>
// /* <td>{getDateTime(appointment.date_time).time}</td> */
