import './AddAppointment.css'

export default function AddAppointment() {
    return (
<form className="form">
    <h1>Create a service appointment</h1>

  <div class="mb-3">
    <label>Automobile VIN</label>
    <input class="form-control form-control-lg" type="text"/>
  </div>

  <div class="mb-3">
    <label>Customer</label>
    <input class="form-control form-control-lg" type="text"/>
  </div>

  <div class="mb-3">
  <label>Date</label>
  <input class="form-control form-control-lg" type="date"/>
  </div>

  <div class="mb-3">
  <label>Time</label>
  <input class="form-control form-control-lg" type="time"/>
  </div>

  <div class="mb-3">
  <label>Technician</label>
  <input class="form-control form-control-lg" type="text" placeholder="Choose a technician..."/>
  </div>

  <div class="mb-3">
  <label>Reason</label>
  <input class="form-control form-control-lg" type="text"/>
  </div>

  <button type="submit" class="btn btn-primary">Create</button>
</form>
    )
}
