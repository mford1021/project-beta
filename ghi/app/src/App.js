import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddTechnician from './TechnicianJS/AddTechnician';
import AddAppointment from './TechnicianJS/AddAppointment';
import TechnicianList from './TechnicianJS/TechnicianList';
import AppointmentList from './TechnicianJS/AppointmentList';
import NewCustomerForm from './Sales/NewCustomerForm';
import ListCustomers from './Sales/ListCustomers';
import NewSalesPersonForm from './Sales/NewSalesPersonForm';
import ListSalesPeople from './Sales/ListSalesPeople';
import SalesPersonHistory from './Sales/SalesPersonHistory';
import NewSaleForm from './Sales/NewSale';
import ListSale from './Sales/ListSales';
import NewManufacturerForm from './Inventory/NewManufacturer';
import ManufacturerList from './Inventory/ManufacturerList';
import NewAutomobileModelForm from './Inventory/CreateAutomobileModel';
import ModelList from './Inventory/VehicleModels';
import NewAutomobileForm from './Inventory/CreateAutomobile';
import AutomobileList from './Inventory/AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/create" element={<AddTechnician />} />
          <Route path="/appointments/create" element={<AddAppointment />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/customers/new" element={<NewCustomerForm /> } />
          <Route path="/customers/list" element={<ListCustomers /> } />
          <Route path="/salespeople/new" element={<NewSalesPersonForm /> } />
          <Route path="/salespeople/list" element={<ListSalesPeople /> } />
          <Route path="/salespeople/history" element={<SalesPersonHistory /> } />
          <Route path="/sales/new" element={<NewSaleForm /> } />
          <Route path="/sales/list" element={<ListSale /> } />
          <Route path="/manufacturers" element={<ManufacturerList /> } />
          <Route path="/manufacturers/new" element={<NewManufacturerForm /> } />
          <Route path="/models" element={<ModelList /> } />
          <Route path="/models/new" element={<NewAutomobileModelForm /> } />
          <Route path="/automobiles" element={<AutomobileList /> } />
          <Route path="/automobiles/new" element={<NewAutomobileForm /> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
