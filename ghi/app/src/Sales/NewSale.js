import React, { useEffect, useState } from "react";

function SaleForm () {
    const[automobiles, setAutomobiles] = useState([]);
    const[sales_people, setSalesPeople] = useState([]);
    const[customers, setCustomers] = useState ([]);
    const[formData, setFormData] = useState({
        automobile: '',
        customer: '',
        sales_person: '',
        price_field: '',
    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (salesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                price_field: '',
                automobile: '',
                sales_person: '',
                customer: '',
            });
        }
    }

    const fetchAutomobile = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)

        }
    }

    const fetchSalesPerson = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.sales_people)
        }
    }

    const fetchCustomer = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchCustomer();
        fetchSalesPerson();
        fetchAutomobile();
    }, []);


    return (
        <div className="my-5 container">
        <div className="row">
            <form onSubmit={handleSubmit}
                id="new-sale-form">
                <h1 className="mb-3">
                    Register new Sale
                </h1>
                <div className="col">
                    <div className="form-floating mb-3">
                        <input style={{ backgroundColor: "rgb(228, 230, 240)"}}
                        onChange={handleFormChange}
                        value={formData.price_field}
                        required placeholder="price_field"
                        type="integer"
                        id="price_field"
                        name="price_field"
                        className="form-control"/>
                        <label htmlFor="Price">Price</label>
                    </div>
                </div>
                <div className="mb-3">
                    <select style={{ backgroundColor: "rgb(228, 230, 240)"}}
                    onChange={handleFormChange}
                    value={formData.automobile}
                    name="automobile"
                    id="automobile"
                    className="btn-lg dropdown-toggle"
                    required>
                        <option value="">Choose an Automobile</option>
                        {automobiles.map(automobile => {
                            return (
                                <option key={automobile.id} value={automobile.id}>
                                    {automobile.vin}
                                </option>
                            );
                            })}
                    </select>
                </div>
                <div className="mb-3">
                    <select style={{ backgroundColor: "rgb(228, 230, 240)"}}
                    onChange={handleFormChange}
                    value={formData.sales_person}
                    name="sales_person"
                    id="sales_person"
                    className="btn-lg dropdown-toggle"
                    required>
                        <option>Choose a Sales Person</option>
                        {sales_people.map(sales_person => {
                            return (
                                <option key={sales_person.id} value={sales_person.id}>
                                    {sales_person.first_name}
                                </option>
                            );
                            })}
                    </select>
                </div>
                <div className="mb-3">
                    <select style={{ backgroundColor: "rgb(228, 230, 240)"}}
                    onChange={handleFormChange}
                    value={formData.customer}
                    name="customer"
                    id="customer"
                    className="btn-lg dropdown-toggle"
                    required>
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name}
                                </option>
                            );
                            })}
                    </select>
                </div>
                <button className="btn btn-lg btn-secondary">Create!</button>
            </form>
        </div>
    </div>
)
};

export default SaleForm;
