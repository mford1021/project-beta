import React, { useState, useEffect } from "react";

function ListSalesPeople() {
    const [salespeople, setSalesPerson] = useState([]);
    const fetchData = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setSalesPerson(data.sales_people);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const deleteSalesPerson = async(id) => {
        const url = `http://localhost:8090/api/salespeople/${id}`;
        const response = await fetch(url, {method: "DELETE"});
        if (response.ok) {
            setSalesPerson(salespeople.filter(salesperson => salesperson.id !== id));
        }
    };


    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Sales People</h1>
                <table className="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {salespeople?.map((salesperson) => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                            <td>
                                <button className="btn btn-secondary" onClick={() => deleteSalesPerson(salesperson.id)}>Remove</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
        </div>
    </div>
)}

export default ListSalesPeople;
