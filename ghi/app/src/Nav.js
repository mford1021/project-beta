import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/customers/new">Create a new Customer</NavLink>
              <NavLink className="nav-link" to="/customers/list">List of all Customers</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/salespeople/new">Create a new Sales Person</NavLink>
              <NavLink className="nav-link" to="/salespeople/list">List of all Sales People</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/salespeople/history">Sales Rep History</NavLink>
              <NavLink className="nav-link" to="/appointments/create">Create an Appointment</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/appointments">Appointments</NavLink>
              <NavLink className="nav-link" to="/appointments/history">Service History</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/technicians/create">Hire New Technician</NavLink>
              <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/models/new">New Automobile Model</NavLink>
              <NavLink className="nav-link" to="/models">Models</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/automobiles/new">New Automobile</NavLink>
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/manufacturers/new">Add a Manufacturer</NavLink>
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className = "navbar navbar-nav">
              <NavLink className="nav-link" to="/sales/new">Register a Sale</NavLink>
              <NavLink className="nav-link" to="/sales/list">List of all Sales</NavLink>
            </li>
        </div>
    </nav>
  )
}

export default Nav;
